require 'ostruct'

class PublicStaticsApiController < ApplicationController

  skip_before_filter :check_if_login_required, :check_password_change

  def index
    #exemple
    @result = ActiveRecord::Base.connection.exec_query('SELECT * FROM statistics')
    @statistics = []

    @result.each do |item|
      statistic = OpenStruct.new
      statistic.ust = item['ust']
      statistic.point_function = item['point_function']
      statistic.available = item['available']
      statistic.total = item['total']
      @statistics.push(statistic)
    end

    respond_to :json

  end
end
