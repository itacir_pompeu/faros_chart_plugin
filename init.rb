Redmine::Plugin.register :faros_chart_plugin do
  name 'Faros Chart Plugin plugin'
  author 'Foros Educacional Team (Itacir F Pompeu)'
  description 'Rest Charts Plugins'
  version '0.0.1'
  url 'http://incoming.com'
  author_url 'www.faroseducacional.com.br'
end
