require File.expand_path('../../test_helper', __FILE__)

class PublicStaticsApiController < ActionController::TestCase
  # Replace this with your real tests.
  def test_get_statics
    get "/public/statics.json"
    assert_response :success
  end
end
