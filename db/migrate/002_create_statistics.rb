class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :ust
      t.integer :point_function
      t.integer :available
      t.integer :total
    end
  end
end
