class CreateJsonStrings < ActiveRecord::Migration
  def change
    create_table :json_strings do |t|
      t.string :json
    end
  end
end
